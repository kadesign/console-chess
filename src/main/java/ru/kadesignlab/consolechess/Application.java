package ru.kadesignlab.consolechess;

import java.util.Scanner;

/*
 * TODO:
 *  1. Check if there is a "check" or "checkmate" situation
 *  2. Rook should be able to do a castling if possible
 *  3. Another OPs
 */
public class Application
{
  static Scanner SCANNER=new Scanner(System.in);

  public static void main(String[] args)
  {
    boolean isRunning=true;

    while (isRunning)
    {
      Game game=new Game();
      game.start();
      isRunning=game.isNewGameWanted();
    }
  }
}
