package ru.kadesignlab.consolechess;

import ru.kadesignlab.consolechess.exceptions.IllegalMoveException;

public class MoveParser
{
  /**
   *
   * @param move
   * @return int[] Row, Col
   * @throws IllegalMoveException
   */
  public static int[] parse(String move) throws IllegalMoveException
  {
    if (move.equals("") || move.length() != 2
        || move.charAt(0) < 'A' || move.charAt(0) > 'z'
        || move.charAt(1) < '1' || move.charAt(1) > '8')
    {
      throw new IllegalMoveException("There is no such square on board!");
    }

    int row=Integer.parseInt(String.valueOf(move.charAt(1))) - 1;
    int col;

    switch(move.charAt(0))
    {
      case 'a':
      case 'A':
        col = 0;
        break;
      case 'b':
      case 'B':
        col = 1;
        break;
      case 'c':
      case 'C':
        col = 2;
        break;
      case 'd':
      case 'D':
        col = 3;
        break;
      case 'e':
      case 'E':
        col = 4;
        break;
      case 'f':
      case 'F':
        col = 5;
        break;
      case 'g':
      case 'G':
        col = 6;
        break;
      case 'h':
      case 'H':
        col = 7;
        break;
      default:
        throw new IllegalMoveException("There is no such square on board!");
    }

    return new int[] { row, col };
  }
}
