package ru.kadesignlab.consolechess.board;

public class Graphics
{
  public static String BOARD_TOP =  " \t A\t B\t C\t D\t E\t F\t G\t H\t \t\n"
                                  + " \t┍\t┳\t┳\t┳\t┳\t┳\t┳\t┳\t┓\t ";
  public static String BOARD_BOTTOM=" \t┗\t┻\t┻\t┻\t┻\t┻\t┻\t┻\t┛\t \n"
                                  + " \t A\t B\t C\t D\t E\t F\t G\t H\t \t";
  public static String SQUARE_SEPARATOR="┃";
  public static String ROW_SEPARATOR=" \t┣\t╋\t╋\t╋\t╋\t╋\t╋\t╋\t┫\t ";
}
