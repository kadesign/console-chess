package ru.kadesignlab.consolechess.board;

import ru.kadesignlab.consolechess.pieces.*;

import static ru.kadesignlab.consolechess.board.Graphics.*;

public class Board
{
  Square[][] squares=new Square[8][8];

  public Board()
  {
    for (int row=0; row < 8; row++)
    {
      for (int col=0; col < 8; col++)
      {
        squares[row][col]=new Square();
      }
    }

    this.init();
  }

  public void init() {
    // White
    for (Square square : squares[1])
    {
      square.setPiece(new Pawn(true));
    }
    squares[0][0].setPiece(new Rook(true));
    squares[0][7].setPiece(new Rook(true));
    squares[0][1].setPiece(new Knight(true));
    squares[0][6].setPiece(new Knight(true));
    squares[0][2].setPiece(new Bishop(true));
    squares[0][5].setPiece(new Bishop(true));
    squares[0][3].setPiece(new Queen(true));
    squares[0][4].setPiece(new King(true));

    // Black
    for (Square square : squares[6])
    {
      square.setPiece(new Pawn(false));
    }
    squares[7][0].setPiece(new Rook(false));
    squares[7][7].setPiece(new Rook(false));
    squares[7][1].setPiece(new Knight(false));
    squares[7][6].setPiece(new Knight(false));
    squares[7][2].setPiece(new Bishop(false));
    squares[7][5].setPiece(new Bishop(false));
    squares[7][3].setPiece(new Queen(false));
    squares[7][4].setPiece(new King(false));

    // Empty squares
    for (int i=2; i < squares.length - 2; i++) {
      for (Square square : squares[i])
      {
        square.setPiece(null);
      }
    }
  }

  public void draw()
  {
    System.out.println(BOARD_TOP);

    for (int i=squares.length - 1; i >= 0; i--)
    {
      System.out.print((i + 1) + "\t" + SQUARE_SEPARATOR);

      for (int j=0; j < squares[i].length; j++) {
        AbstractPiece piece = squares[i][j].getPiece();
        if (piece != null) {
          piece.draw();
        }
        System.out.print("\t" + SQUARE_SEPARATOR);
      }
      System.out.println("\t" + (i + 1));

      if (i != 0)
        System.out.println(ROW_SEPARATOR);
    }

    System.out.println(BOARD_BOTTOM);
  }

  public Square getSquare(int row, int col)
  {
    return squares[row][col];
  }

  public int getCountOfPieces(boolean white)
  {
    int count=0;

    for (Square[] row : squares)
    {
      for (Square square : row)
      {
        AbstractPiece piece = square.getPiece();
        if (piece != null && piece.isWhite() == white)
        {
          count++;
        }
      }
    }

    return count;
  }
}
