package ru.kadesignlab.consolechess.board;

import ru.kadesignlab.consolechess.pieces.AbstractPiece;

public class Square
{
  private AbstractPiece piece;

  public AbstractPiece getPiece()
  {
    return piece;
  }

  public void setPiece(AbstractPiece piece)
  {
    this.piece=piece;
  }

  public void clear()
  {
    piece=null;
  }

  public boolean hasPiece()
  {
    return piece != null;
  }
}
