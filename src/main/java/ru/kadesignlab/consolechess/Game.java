package ru.kadesignlab.consolechess;

import ru.kadesignlab.consolechess.board.Board;
import ru.kadesignlab.consolechess.exceptions.IllegalMoveException;
import ru.kadesignlab.consolechess.pieces.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.kadesignlab.consolechess.Application.SCANNER;

public class Game
{
  public final Board board;

  private boolean isRunning;
  private boolean isWhiteTurn=true;
  private boolean isMoveInProgress;

  private int originRow=-1;
  private int originCol=-1;
  private int destRow=-1;
  private int destCol=-1;

  private AbstractPiece selectedPiece;

  public Game()
  {
    this.board=new Board();
    this.isRunning=true;
  }

  public void redrawBoard()
  {
    this.board.draw();
  }

  public void start()
  {
    isRunning=true;

    while (isRunning)
    {
      move();
    }
  }

  private void end()
  {
    isRunning=false;
  }

  public boolean isNewGameWanted()
  {
    System.out.print("Do you want to start another game? (Yes: y/yes, No: any input): ");
    String in=SCANNER.nextLine();
    return in.equalsIgnoreCase("y") || in.equalsIgnoreCase("yes");
  }

  public void move()
  {
    redrawBoard();

    System.out.println("Turn: " + (isWhiteTurn ? "white" : "black"));
    while (originCol == -1)
    {
      System.out.print("Choose a piece [A1 - H8]: ");
      try
      {
        parseInput(true);
        selectedPiece=this.board.getSquare(originRow, originCol).getPiece();
        if (selectedPiece == null)
        {
          clearMove(true);
          throw new IllegalMoveException("This square is empty!");
        }
        if ((selectedPiece.isWhite() && !isWhiteTurn) || (!selectedPiece.isWhite() && isWhiteTurn))
        {
          clearMove(true);
          throw new IllegalMoveException("This piece is not yours!");
        }
        if (!canMove(selectedPiece, originRow, originCol))
        {
          clearMove(true);
          throw new IllegalMoveException("This piece can't move now, choose another one!");
        }

        System.out.println("Chosen: " + selectedPiece.getClass().getSimpleName() + " (" + (isWhiteTurn ? "white" : "black") + ")");
      }
      catch (IllegalMoveException e)
      {
        System.out.println(e.getMessage());
      }
    }

    while (destCol == -1)
    {
      System.out.print("Where to move the piece to? [A1 - H8]: ");
      try
      {
        parseInput(false);

        if (!selectedPiece.isMoveValid(originCol, originRow, destCol, destRow) || !checkObstacles(originRow, originCol, destRow, destCol))
        {
          clearMove(false);
          throw new IllegalMoveException("This piece can't move to this square!");
        }

        AbstractPiece destPiece=board.getSquare(destRow, destCol).getPiece();
        if (destPiece != null)
        {
          if (destPiece.isWhite() == selectedPiece.isWhite())
          {
            clearMove(false);
            throw new IllegalMoveException("There is your piece on this square!");
          }
          else
          {
            System.out.println("You have captured opponent's " + destPiece.getClass().getSimpleName() + "!");
          }
        }
        else
        {
          if (selectedPiece instanceof Pawn && Math.abs(originCol - destCol) != 0)
          {
            clearMove(false);
            throw new IllegalMoveException("There is no opponent's piece on this square!");
          }
        }

        board.getSquare(destRow, destCol).setPiece(selectedPiece);
        board.getSquare(originRow, originCol).clear();
      }
      catch (IllegalMoveException e)
      {
        System.out.println(e.getMessage());
      }
    }

    clearMove(true);
    clearMove(false);

    if (board.getCountOfPieces(true) == 0 || board.getCountOfPieces(false) == 0)
    {
      end();
    }

    isWhiteTurn=!isWhiteTurn;
  }

  private void clearMove(boolean isSourceMove)
  {
    if (isSourceMove)
    {
      originCol=-1;
      originRow=-1;
      selectedPiece=null;
    }
    else
    {
      destCol=-1;
      destRow=-1;
    }
  }

  private void parseInput(boolean isSourceMove) throws IllegalMoveException
  {
    String in=SCANNER.nextLine();
    int[] parsedMove=MoveParser.parse(in);
    if (isSourceMove)
    {
      originRow=parsedMove[0];
      originCol=parsedMove[1];
    }
    else
    {
      destRow=parsedMove[0];
      destCol=parsedMove[1];
    }
  }

  private boolean checkObstacles(final int originRow, final int originCol, final int destRow, final int destCol)
  {
    // Special check for knight's move
    if ((Math.abs(originRow - destRow) == 2 && Math.abs(originCol - destCol) == 1)
     || (Math.abs(originRow - destRow) == 1 && Math.abs(originCol - destCol) == 2))
      return true;

    int checkedRow=originRow;
    int checkedCol=originCol;

    do
    {
      if (checkedRow != destRow)
      {
        checkedRow=(destRow - checkedRow > 0) ? ++checkedRow : --checkedRow;
      }
      if (checkedCol != destCol)
      {
        checkedCol=(destCol - checkedCol > 0) ? ++checkedCol : --checkedCol;
      }

      if (board.getSquare(checkedRow, checkedCol).hasPiece() && board.getSquare(checkedRow, checkedCol).getPiece().isWhite() == selectedPiece.isWhite())
        return false;
    }
    while (checkedRow != destRow && checkedCol != destCol);

    return true;
  }

  private boolean canMove(final AbstractPiece piece, final int row, final int col)
  {
    int[][] lShapedMoves = { {1, 2}, {2, 1}, {-1, -2}, {-2, -1}, {1, -2}, {2, -1}, {-1, 2}, {-2, 1} };
    int[][] straightMoves = { {0, 1}, {1, 0}, {0, -1}, {-1, 0} };
    int[][] diagonalMoves = { {1, 1}, {1, -1}, {-1, 1}, {-1, -1} };
    int[][] whitePawnMoves = { {1, 0}, {1, 1}, {1, -1} };
    int[][] blackPawnMoves = { {-1, 0}, {-1, 1}, {-1, -1} };

    List<int[]> checkedMoves = new ArrayList<>();

    if (piece instanceof Bishop || piece instanceof King || piece instanceof Queen) checkedMoves.addAll(Arrays.asList(diagonalMoves));
    if (piece instanceof King || piece instanceof Queen || piece instanceof Rook) checkedMoves.addAll(Arrays.asList(straightMoves));
    if (piece instanceof Knight) checkedMoves.addAll(Arrays.asList(lShapedMoves));
    if (piece instanceof Pawn)
    {
      checkedMoves.addAll(piece.isWhite() ? Arrays.asList(whitePawnMoves) : Arrays.asList(blackPawnMoves));
      for (int[] move : checkedMoves)
      {
        int checkedRow = row + move[0];
        int checkedCol = col + move[1];

        if (isMoveOutOfBoard(checkedRow, checkedCol)) continue;

        if (move[1] == 0)
        {
          if (!board.getSquare(checkedRow, checkedCol).hasPiece()) return true;
        }
        else
        {
          if (board.getSquare(checkedRow, checkedCol).hasPiece() && board.getSquare(checkedRow, checkedCol).getPiece().isWhite() != piece.isWhite())
            return true;
        }
      }

      return false;
    }

    for (int[] move : checkedMoves)
    {
      int checkedRow = row + move[0];
      int checkedCol = col + move[1];
      if (isMoveOutOfBoard(checkedRow, checkedCol)) continue;
      if (!board.getSquare(checkedRow, checkedCol).hasPiece() || board.getSquare(checkedRow, checkedCol).getPiece().isWhite() != piece.isWhite())
        return true;
    }

    return false;
  }

  private boolean isMoveOutOfBoard (int row, int col)
  {
    return row < 0 || row > 7 || col < 0 || col > 7;
  }
}
