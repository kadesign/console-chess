package ru.kadesignlab.consolechess.pieces;

/**
 * Конь
 */
public class Knight extends AbstractPiece
{
  public Knight(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2658" : "\u265E");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    return (Math.abs(srcCol - dstCol) == 2 && Math.abs(srcRow - dstRow) == 1)
        || (Math.abs(srcCol - dstCol) == 1 && Math.abs(srcRow - dstRow) == 2);
  }
}
