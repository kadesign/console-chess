package ru.kadesignlab.consolechess.pieces;

/**
 * Пешка
 */
public class Pawn extends AbstractPiece
{
  public Pawn(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2659" : "\u265F");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    if (this.isWhite())
    {
      return (((srcCol == dstCol) && dstRow == (srcRow + 1))
          || (srcRow == 1 && dstCol == srcCol && dstRow == (srcRow + 2))
          || (dstRow == (srcRow + 1) && Math.abs(srcCol - dstCol) == 1));
    }
    else
    {
      return (((srcCol == dstCol) && dstRow == (srcRow - 1))
          || (srcRow == 6 && dstCol == srcCol && dstRow == (srcRow - 2))
          || (dstRow == (srcRow - 1) && Math.abs(srcCol - dstCol) == 1));
    }
  }
}
