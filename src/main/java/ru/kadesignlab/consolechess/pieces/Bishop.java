package ru.kadesignlab.consolechess.pieces;

/**
 * Слон
 */
public class Bishop extends AbstractPiece
{
  public Bishop(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2657" : "\u265D");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    return Math.abs(srcCol - dstCol) == Math.abs(srcRow - dstRow);
  }
}
