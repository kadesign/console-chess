package ru.kadesignlab.consolechess.pieces;

/**
 * Король
 */
public class King extends AbstractPiece
{
  public King(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2654" : "\u265A");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    return Math.abs(srcCol - dstCol) <= 1 || Math.abs(srcRow - dstRow) <= 1;
  }
}
