package ru.kadesignlab.consolechess.pieces;

/**
 * Ладья
 */
public class Rook extends AbstractPiece
{
  public Rook(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2656" : "\u265C");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    return srcCol == dstCol ^ srcRow == dstRow;
  }
}
