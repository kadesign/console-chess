package ru.kadesignlab.consolechess.pieces;

public abstract class AbstractPiece
{
  private final boolean isWhite;

  public AbstractPiece(boolean isWhite) {
    this.isWhite = isWhite;
  }

  public boolean isWhite()
  {
    return isWhite;
  }

  public abstract void draw();

  public abstract boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow);
}
