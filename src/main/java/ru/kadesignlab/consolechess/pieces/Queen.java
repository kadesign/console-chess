package ru.kadesignlab.consolechess.pieces;

/**
 * Ферзь
 */
public class Queen extends AbstractPiece
{
  public Queen(boolean isWhite)
  {
    super(isWhite);
  }

  @Override
  public void draw()
  {
    System.out.print(this.isWhite() ? "\u2655" : "\u265B");
  }

  @Override
  public boolean isMoveValid(int srcCol, int srcRow, int dstCol, int dstRow)
  {
    return (Math.abs(srcCol - dstCol) == Math.abs(srcRow - dstRow)) || (srcCol == dstCol ^ srcRow == dstRow);
  }
}
