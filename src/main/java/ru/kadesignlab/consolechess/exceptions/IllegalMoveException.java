package ru.kadesignlab.consolechess.exceptions;

public class IllegalMoveException extends Exception
{
  public IllegalMoveException(String message)
  {
    super(message);
  }
}
